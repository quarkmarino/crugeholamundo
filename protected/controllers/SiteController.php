<?php

class SiteController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		$this->render('index');
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
	    if($error=Yii::app()->errorHandler->error)
	    {
	    	if(Yii::app()->request->isAjaxRequest)
	    		echo $error['message'];
	    	else
	        	$this->render('error', $error);
	    }
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$headers="From: {$model->email}\r\nReply-To: {$model->email}";
				mail(Yii::app()->params['adminEmail'],$model->subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				$this->redirect(Yii::app()->user->returnUrl);
		}
		// display the login form
		$this->render('login',array('model'=>$model));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
	
	public function actionTester(){
		$this->render('tester');
	}
	
	
	public function actionAjaxCrearUsuario(){
		// asi se crea un usuario (una nueva instancia en memoria volatil)
		$usuarioNuevo = Yii::app()->user->um->createBlankUser(); 
		
		$usuarioNuevo->username = 'username1';
		$usuarioNuevo->email = 'username1@gmail.com';
		// la establece como "Activada"
		Yii::app()->user->um->activateAccount($usuarioNuevo);
		
		if(Yii::app()->user->um->loadUser($usuarioNuevo->username) != null)
			{
				echo "El usuario {$usuarioNuevo->username} ya ha sido creado.";
				return;
			}
		
		// ponerle una clave
		Yii::app()->user->um->changePassword($usuarioNuevo,"123456");
		
		// guarda usando el API, la cual hace pasar al usuario por el sistema de filtros.
		if(Yii::app()->user->um->save($usuarioNuevo)){
		
			echo "Usuario creado: id=".$usuarioNuevo->primaryKey;
		}
		else{
			$errores = CHtml::errorSummary($usuarioNuevo);
		
			echo "no se pudo crear el usuario: ".$errores;
		}
	}
}